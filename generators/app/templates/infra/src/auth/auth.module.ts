import { Module } from '@nestjs/common';
import { ApiKeyStrategy } from './apikey.strategy';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [PassportModule],
  providers: [ApiKeyStrategy],
})
export class AuthModule {}
