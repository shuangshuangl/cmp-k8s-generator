'use strict';
const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');

module.exports = class extends Generator {

  async prompting() {
    // Have Yeoman greet the user.
    this.log(
      yosay(
        `Welcome to CMP ${chalk.red('k8s-nestjs-services')} generator!`
      )
    );

    const projectTypes = [
      {
        name: 'infra',
        value: 'infra'
      },
      {
        name: 'data',
        value: 'data'
      },      
      {
        name: 'gql',
        value: 'gql'
      },
    ];

    const answerProjectType = await this.prompt([
      {
        type: 'list',
        name: 'projectType',
        message: 'Which group/modules does your NestJS services belong to?',
        choices: projectTypes
      }
    ]);

    const questionsProjectInfo = [
      {
        type: "input",
        name: "name",
        message: "What is the name of your project?",
        default: `CMP-awesome-services`
      },
      {
        type: "input",
        name: "description",
        message: "Give us some descriptions of your project",
        default: ""
      },
      {
        type: "input",
        name: "author",
        message: "Who is the author of this project?",
        store: true
      }

    ];

    const answerProjectInfo = await this.prompt(questionsProjectInfo);
    answerProjectInfo.name = answerProjectInfo.name.replace(/\s+/g, '-').toLowerCase();

    const dbTypes = [
      {
        name: 'dynamo',
        value: 'dynamo'
      },
      {
        name: 'opensearch',
        value: 'opensearch'
      },
      {
        name: 'None',
        value: 'none'
      }
    ];

    const questionProjectLib = [
      {
        type: 'list',
        name: 'dbType',
        message: 'which database do you want to use?',
        choices: dbTypes
      }
    ];

    const businessDomains = [
      {
        name: 'carrier',
        value: 'carrier'
      },
      {
        name: 'vendor',
        value: 'vendor'
      },
      {
        name: 'device',
        value: 'device'
      },
      {
        name: 'lifecycle',
        value: 'lifecycle'
      },
      {
        name: 'utility',
        value: 'utility'
      },
      {
        name: 'None',
        value: 'none'
      }
    ];

    if(answerProjectType.projectType === 'infra') {
      questionProjectLib.push({
        type: "list",
        name: "businessDomain",
        message: "Under CMP Infra, which business domain does your service belong to?",
        choices: businessDomains
      });
    }

    // if (answerProjectType.projectType !== 'gql') {
    //   questionProjectLib.push({
    //     type: "input",
    //     name: "useKafka",
    //     message: "Do you want to use Kafka? (Y/N)",
    //     default: "N"
    //   });
    // }
    const answerProjectLib = await this.prompt(questionProjectLib);

    this.props = {
      answerProjectType,
      answerProjectInfo,
      answerProjectLib
    };
    // return this.prompt(prompts).then(props => {
    //   // To access props later use this.props.someAnswer;
    //   this.props = props;
    // });
  }

  writing() {
    console.log("All questions are done, start generating files...");
    const pkgJson = {
      devDependencies: {},
      dependencies: {}
    };

    const globalConfig = {
      name: this.props.answerProjectInfo.name,
      description: this.props.answerProjectInfo.description,
      author: this.props.answerProjectInfo.author,
      //useKafka: this.props.answerProjectInfo.messageBroker && this.props.answerProjectInfo.messageBroker.toLowerCase() === 'kafka',
      appName: this.props.answerProjectInfo.name,
      appDescription: this.props.answerProjectInfo.description,
      appPort: this.props.answerProjectInfo.appPort,
    };

    // Copy main.ts
    this.fs.copyTpl(
      this.templatePath(`${this.props.answerProjectType.projectType}/src/main.ts`),
      this.destinationPath(`${this.props.answerProjectInfo.name}/src/main.ts`),
      globalConfig
      );

    // Copy jest file
    this.fs.copyTpl(
      this.templatePath(`${this.props.answerProjectType.projectType}/jest/**/*`),
      this.destinationPath(`${this.props.answerProjectInfo.name}/jest`),
      globalConfig
    );

    this.fs.copyTpl(
      this.templatePath(`${this.props.answerProjectType.projectType}/.dockerignore`),
      this.destinationPath(`${this.props.answerProjectInfo.name}/.dockerignore`),
      globalConfig
     );

     this.fs.copyTpl(
      this.templatePath(`${this.props.answerProjectType.projectType}/.envrc`),
      this.destinationPath(`${this.props.answerProjectInfo.name}/.envrc`),
      globalConfig
     );

     this.fs.copyTpl(
      this.templatePath(`${this.props.answerProjectType.projectType}/.eslintrc.js`),
      this.destinationPath(`${this.props.answerProjectInfo.name}/.eslintrc.js`),
      globalConfig
     );

     this.fs.copyTpl(
      this.templatePath(`${this.props.answerProjectType.projectType}/.gitignore`),
      this.destinationPath(`${this.props.answerProjectInfo.name}/.gitignore`),
      globalConfig
     );

     this.fs.copyTpl(
      this.templatePath(`${this.props.answerProjectType.projectType}/.gitlab-ci.yml`),
      this.destinationPath(`${this.props.answerProjectInfo.name}/.gitlab-ci.yml`),
      globalConfig
     );

     this.fs.copyTpl(
      this.templatePath(`${this.props.answerProjectType.projectType}/.npmrc`),
      this.destinationPath(`${this.props.answerProjectInfo.name}/.npmrc`),
      globalConfig
     );

     this.fs.copyTpl(
      this.templatePath(`${this.props.answerProjectType.projectType}/.prettierignore`),
      this.destinationPath(`${this.props.answerProjectInfo.name}/.prettierignore`),
      globalConfig
     );

     this.fs.copyTpl(
      this.templatePath(`${this.props.answerProjectType.projectType}/.prettierrc.json`),
      this.destinationPath(`${this.props.answerProjectInfo.name}/.prettierrc.json`),
      globalConfig
     );

     this.fs.copyTpl(
      this.templatePath(`${this.props.answerProjectType.projectType}/app-properties.yml`),
      this.destinationPath(`${this.props.answerProjectInfo.name}/app-properties.yml`),
      globalConfig
     );

     this.fs.copy(
      this.templatePath(`${this.props.answerProjectType.projectType}/CODEOWNERS`),
      this.destinationPath(`${this.props.answerProjectInfo.name}/CODEOWNERS`)
     );
 
     this.fs.copyTpl(
      this.templatePath(`${this.props.answerProjectType.projectType}/README.md`),
      this.destinationPath(`${this.props.answerProjectInfo.name}/README.md`),
      globalConfig
     );
 
     this.fs.copyTpl(
      this.templatePath(`${this.props.answerProjectType.projectType}/package.json`),
      this.destinationPath(`${this.props.answerProjectInfo.name}/package.json`),
      globalConfig
     );

     this.fs.copy(
      this.templatePath(`${this.props.answerProjectType.projectType}/sonar-project.properties`),
      this.destinationPath(`${this.props.answerProjectInfo.name}/sonar-project.properties`)
     );
 
     this.fs.copy(
      this.templatePath(`${this.props.answerProjectType.projectType}/tsconfig.json`),
      this.destinationPath(`${this.props.answerProjectInfo.name}/tsconfig.json`)
     );
 
     this.fs.copy(
      this.templatePath(`${this.props.answerProjectType.projectType}/tsconfig.build.json`),
      this.destinationPath(`${this.props.answerProjectInfo.name}/tsconfig.build.json`)
     );

     this.fs.copy(
      this.templatePath(`${this.props.answerProjectType.projectType}/VERSION`),
      this.destinationPath(`${this.props.answerProjectInfo.name}/VERSION`)
     );
     
  }

  install() {
    if (this.options['skip-install']) {
      this.log(chalk.green(`
        To install dependencies, run
        ${chalk.white('$')} cd ${this.props.answerProjectInfo.name}/
        ${chalk.white('$')} yarn install
      `));
    } else {
      var npmdir = `${process.cwd()}/${this.props.answerProjectInfo.name}`;
      this.yarnInstall([], {}, {cwd: npmdir});
    }
  }

  end() {
    if (!this.options['skip-install']) {
      var npmdir = `${process.cwd()}/${this.props.answerProjectInfo.name}`;
      this.spawnCommand('npm', ['run', 'lint'], {cwd: npmdir});
    }
  }
};
