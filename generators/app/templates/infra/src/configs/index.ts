import {
    generateEnv,
    envStringValidator,
    envPortValidator,
    envCustomValidator,
    envNumberValidator,
  } from '@rivian/dt-environment-validation-lib';
  
  import { LogLevel } from '../types';
  
  const jsonValidator = envCustomValidator((input: string) => {
    return JSON.parse(input);
  });
  
  const envSpecs = {
    API_KEY_HEADER: envStringValidator({
      default: 'x-api-key',
    }),
    API_KEY: envStringValidator({
      default: '9192d0faa740d0af32e66cd576b5dbdc',
    }),
    LOG_LEVEL: envStringValidator({
      choices: Object.values(LogLevel),
      devDefault: LogLevel.Debug,
      default: LogLevel.Info,
    }),
    APP_NAME: envStringValidator({
      default: '',
    }),
    APP_PORT: envPortValidator({ default: 3000 }),
    DT_CMP_ES_REALTIME_URL: envStringValidator({
      default:
        'https://vpc-cmp-dynamo-es-stream-3ovs2x756lxdcaifdf2p4utoom.us-east-1.es.amazonaws.com',
    }),
    DT_CMP_ES_REALTIME_INDEX: envStringValidator({
      default: 'v2.cmp.device.online.map',
    }),
    H3_RESOLUTIONS: jsonValidator({
      default: [0, 1, 2, 3, 4, 5, 8],
    }),
    DYNAMO_HISTORY_TABLE: envStringValidator({
      default: 'DT_CMP_REALTIME_HISTORY',
    }),
    HISTORY_RETENTION_PERIOD: envNumberValidator({
      default: 15_778_476, // 6 months in seconds
    }),
    MAX_HEXES_RETURN: envNumberValidator({
      default: 10_000,
    }),
    GLOBAL_AGG_CACHE_TTL: envNumberValidator({
      default: 60,
    }),
    AWS_REGION: envStringValidator({
      default: 'us-east-1'
    })
  };
  
  console.log('get specs')
  export const env = generateEnv(envSpecs);
  