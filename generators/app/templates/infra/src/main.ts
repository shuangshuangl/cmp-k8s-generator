import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { env } from './configs';
import { AppUtils } from './utils/app.utils';
import bodyParser from 'body-parser';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {});

  app.enableCors({});
  app.use(bodyParser.json({limit: '50mb'})); 
  app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
  await app.startAllMicroservices();
  await app.listen(env.APP_PORT);

  AppUtils.killAppWithGrace(app);
}
bootstrap();
