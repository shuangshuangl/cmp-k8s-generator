import { Test, TestingModule } from '@nestjs/testing';
import { ApiKeyStrategy } from './apikey.strategy';
import { env } from '../configs';
import { UnauthorizedException } from '@nestjs/common';

describe('AppController', () => {
  let apiKeyStr: ApiKeyStrategy;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [ApiKeyStrategy],
      providers: [],
    }).compile();
    apiKeyStr = app.get<ApiKeyStrategy>(ApiKeyStrategy);
  });

  afterEach(() => {
    jest.clearAllMocks();
  })

  describe('ApiKeyStrategy', () => {
    it('should verify api-key in header', () => {
        const strategy = new ApiKeyStrategy();
        expect(strategy.apiKeyHeader.header).toEqual(env.API_KEY_HEADER);
        strategy.verify(env.API_KEY, (err) => {
            expect(err).toBe(null)
        });
        strategy.verify('Something wrong', (err) => {
            expect(err).toBeInstanceOf(UnauthorizedException)
        })
    })
  });
});
