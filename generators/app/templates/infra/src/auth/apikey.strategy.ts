import { HeaderAPIKeyStrategy } from 'passport-headerapikey';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { env } from '../configs';

@Injectable()
export class ApiKeyStrategy extends PassportStrategy(HeaderAPIKeyStrategy, 'api-key') {
  constructor() {
    super({ header: env.API_KEY_HEADER, prefix: '' }, true, (apiKey: string, verified: any) => {
      if (apiKey !== env.API_KEY) {
        return verified(new UnauthorizedException(), null);
      }
      return verified(null, true);
    });
  }
}
