module.exports = {
    root: true,
    parser: "@typescript-eslint/parser",
    ignorePatterns: [".eslintrc.js", "**/*.setup.ts", "**/*.config.js"],
    plugins: ["import", "@typescript-eslint"],
    extends: ["airbnb-typescript/base", "plugin:prettier/recommended"], // make sure prettier is last
    parserOptions: {
      project: "./tsconfig.json",
    },
    rules: {
      "import/prefer-default-export": "off",
      "class-methods-use-this": "off",
      "no-underscore-dangle": "off",
    },
  };
  