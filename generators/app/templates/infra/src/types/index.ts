export enum Env {
  Local = 'local',
  Test = 'test',
  Review = 'review',
  Dev = 'development',
  QA = 'qa',
  Staging = 'staging',
  Production = 'production',
}

export enum LogLevel {
  Debug = 'debug',
  Info = 'info',
  Warn = 'warn',
  Error = 'error',
}
  